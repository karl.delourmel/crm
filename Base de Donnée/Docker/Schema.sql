/* Création de la table Client */

create table customers(
    id serial not null,
    lastname varchar(100),
    firstname varchar(100),
    company varchar(200),
    mail varchar(255),
    phone varchar(15),
    notes varchar(255),
    acive boolean,
    PRIMARY key (id)
);

/* Création de la table Utilisateur*/

create table users(
    id serial not null,
    username varchar(30),
    password varchar(255),
    mail varchar(255),
    grants varchar(255),
    primary key(id)
);

/* Création de la table Commandes*/

create table orders(
    id serial not null,
    customer_id int,
    label varchar(100),
    adr_et DECIMAL,
    number_of_days decimal,
    tva decimal,
    status varchar(30),
    type varchar (100),
    notes varchar(255),
    estimate boolean,
    primary key(id),
    foreign key(customer_id) references customers(id)
); 