package fr.wijin.crm.controller;

import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureWebMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import fr.wijin.crm.form.CustomerForm;
import fr.wijin.crm.model.Customer;
import fr.wijin.crm.repository.CustomerRepository;
import jakarta.servlet.http.HttpSession;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureWebMvc
public class CustomerControllerTest {
    
    @Autowired
    @InjectMocks
    private CustomerController customerController;

    @Mock
    CustomerRepository mockCustomerRepository;

    @Mock
    HttpSession mockSession;

    @Mock
    Model mockModel;

    @Mock
    CustomerForm mockCustomerForm;

    @Mock
    BindingResult mockBindingresult;

    @Test
    void testLoadCustomer() {
        Customer customer = new Customer(1, "JONES", "Indiana", "Université de Chicago", "indiana.jonas@univ-chicago.com", "0222222222", "0666666666", "Les notes d''Indiana", true);
        Mockito.when(mockCustomerRepository.findById(1)).thenReturn(Optional.of(customer));
        Assertions.assertEquals("listCustomers", customerController.loadCustomer(1, mockSession));
    }

    @Test
    void testShowForm(){
        Mockito.when(mockSession.getAttribute("role")).thenReturn("ROLE_ADMIN");
        Assertions.assertEquals("listCustomers", customerController.showForm(mockModel, mockSession));

    }

    @Test
    void testShowFormWithCustomerForm() {
        Assertions.assertEquals("createCustomer", customerController.showForm(mockCustomerForm, mockModel));
    }

    @Test
    void testCreateCustomer(){
        Assertions.assertEquals("redirect:/listCustomers", customerController.createCustomer(mockCustomerForm, mockBindingresult, mockModel, mockSession));
    }

    @Test
    void testDeleteCustomer() {
        Assertions.assertEquals("redirect:/listCustomers", customerController.deleteCustomer(1, mockModel, mockSession));
    }

}
