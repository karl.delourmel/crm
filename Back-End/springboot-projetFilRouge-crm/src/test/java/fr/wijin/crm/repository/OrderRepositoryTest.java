package fr.wijin.crm.repository;

import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import fr.wijin.crm.model.Order;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@TestMethodOrder(OrderAnnotation.class)
@DisplayName("Test unitaire OrderRepository")
public class OrderRepositoryTest {
    
    static List<Order> initListOrders = null;
    static Order order = null;

    @Autowired
    private OrderRepository orderRepository;

    @PersistenceContext
    private EntityManager entityManager;

    @BeforeEach
    void setup() {
        initListOrders = orderRepository.findAll();
        order = initListOrders.get(0);
        System.out.println(initListOrders.size());
    }

    @Test
    @DisplayName("Test findAll")
    void testFindAll() {
        Assertions.assertEquals(3, orderRepository.findAll().size());
    }

    @Test
    @DisplayName("Test findByTypeAndStatus")
    void testFindByTypeAndStatus(){
        Assertions.assertEquals(1, orderRepository.findByTypeAndStatus("Forfait", "En cours").size());
    }
}
