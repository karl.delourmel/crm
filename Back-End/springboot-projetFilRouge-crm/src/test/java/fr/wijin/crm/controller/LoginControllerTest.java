package fr.wijin.crm.controller;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureWebMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.ui.Model;


@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureWebMvc
public class LoginControllerTest {
    
    @Autowired
    private LoginController loginController;

    @Mock
    Model mockModel;

    @Test
    void testLogin(){
        Assertions.assertEquals("login", loginController.login("", mockModel));
    }

    @Test
    void testError403(){
        Assertions.assertTrue(loginController.error403() == "error/403");
    }
}
