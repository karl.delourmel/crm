package fr.wijin.crm.form;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

public class UserForm {
	
	private Integer id;
	
	@NotNull
	@Size(max=30)
	private String username;
	
	@NotNull
	@Size(max=255)
	private String password;
	
	@NotNull
	@Size(max=255)
	private String mail;
	
	@NotNull
	@Size(max=255)
	private String grants;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getGrants() {
		return grants;
	}

	public void setGrants(String grants) {
		this.grants = grants;
	}
	
	
	
	

}
