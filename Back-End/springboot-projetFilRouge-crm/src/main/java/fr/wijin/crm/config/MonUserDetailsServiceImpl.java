package fr.wijin.crm.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import fr.wijin.crm.repository.UserRepository;
import fr.wijin.crm.config.MonUserDetails;
import fr.wijin.crm.model.User;

/**
 * Implémentation personnalisée de l'interface UserDetailsService de Spring Security.
 * Cette classe est responsable de la récupération des détails de l'utilisateur depuis la base de données
 * et de la création d'une instance de MonUserDetails pour représenter l'utilisateur dans le système de sécurité.
 */
public class MonUserDetailsServiceImpl implements UserDetailsService {

@Autowired
private UserRepository userRepository;

/**
 * Charge les détails de l'utilisateur à partir de la base de données en utilisant le nom d'utilisateur.
 *
 * @param username Le nom d'utilisateur de l'utilisateur à charger.
 * @return Une instance de UserDetails représentant les détails de l'utilisateur.
 * @throws UsernameNotFoundException Si l'utilisateur n'est pas trouvé dans la base de données.
 */
@Override
public UserDetails loadUserByUsername(String username) 
		throws UsernameNotFoundException {
	if (null == username || username.trim().length() == 0) {
		throw new UsernameNotFoundException("Missing user with no-username " 
				+ username);
	}
	// Récupération depuis la BDD
	User user = userRepository.findByUsername(username);
	if (null == user) {
		throw new UsernameNotFoundException("Missing user with username " 
				+ username);
	}
	return new MonUserDetails(user);
}

}