package fr.wijin.crm.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import fr.wijin.crm.form.OrderForm;
import fr.wijin.crm.model.Customer;
import fr.wijin.crm.model.Order;
import fr.wijin.crm.repository.CustomerRepository;
import fr.wijin.crm.repository.OrderRepository;
import jakarta.servlet.http.HttpSession;

/**
 * Contrôleur gérant les opérations liées aux commandes dans l'application CRM.
 * Ce contrôleur est responsable du traitement des requêtes HTTP liées à la gestion des commandes,
 * telles que l'affichage de la liste des commandes, la création d'une nouvelle commande, la suppression d'une commande, etc.
 */
@Controller
public class OrderController {

	public static final String SESSION_ORDERS = "orders";
	
	public static final String PAGE_ORDERS_LIST = "listOrders";
	public static final String PAGE_ORDER_CREATE = "createOrder";
	
	@Autowired
	private OrderRepository orderRepository;
	
	@Autowired
	private CustomerRepository customerRepository;
	
    /**
     * Obtient un objet à partir de la session en utilisant la clé spécifiée.
     *
     * @param session La session HTTP dans laquelle rechercher l'objet.
     * @param key     La clé de l'objet dans la session.
     * @param <U>     Le type de l'objet.
     * @return L'objet trouvé dans la session ou null s'il n'est pas présent.
     */
	//@SuppressWarnings({ "unchecked", "hiding" })
	private <U> U getFromSession(HttpSession session, String key) {
		Object found = session.getAttribute(key);
		if (null != found) {
			return (U)found;
		}
		return null; 
	}
	
    /**
     * Affiche la liste des commandes.
     *
     * @param model   Le modèle utilisé pour transmettre des données à la vue.
     * @param session La session HTTP utilisée pour stocker des informations temporaires.
     * @return Le nom de la vue à afficher pour la liste des commandes.
     */
	@GetMapping("/listOrders")
	public String welcome(Model model, HttpSession session) {
		
		
			List<Order> orders = orderRepository.findAll();
			
			Map<Integer, Order> ordersMap = new HashMap<>(); 
			for(Order order : orders) {
				ordersMap.put(order.getId(), order);
				System.out.println(ordersMap);
			}
						
			session.setAttribute("orders", ordersMap);
			model.addAttribute(SESSION_ORDERS, orders);
		
		return PAGE_ORDERS_LIST;
	}
	
    /**
     * Supprime une commande spécifiée par son identifiant.
     *
     * @param id      L'identifiant de la commande à supprimer.
     * @param session La session HTTP utilisée pour stocker des informations temporaires.
     * @return Le nom de la vue à afficher après la suppression de la commande.
     */
	@GetMapping("/orders/delete/{orderId}")
	public String deleteOrder(
			@PathVariable("orderId") Integer id, Model model, HttpSession session){
		try {
			Optional<Order> order = orderRepository.findById(id);
			
		if (order.isPresent()) {
			
			orderRepository.delete(order.get());

			Map<Integer, Order> orders = (HashMap<Integer, Order>) session.getAttribute(SESSION_ORDERS);
			// Suppression de la commande de la Map
			orders.remove(id);
			session.removeAttribute("error");
		}
	} catch (Exception e) {
		session.setAttribute("error", "Impossible de supprimer le client!");
	}
		return "redirect:/" + PAGE_ORDERS_LIST;
	}
	
    /**
     * Affiche le formulaire de création d'une nouvelle commande.
     *
     * @param model Le modèle utilisé pour transmettre des données à la vue.
     * @return Le nom de la vue à afficher pour le formulaire de création de commande.
     */
	@GetMapping("/createOrder")
	public String createOrder(Model model) {
		List<Customer> customers = customerRepository.findAll();
		model.addAttribute("orderForm", new OrderForm());
		model.addAttribute("customers", customers);
		return PAGE_ORDER_CREATE;
	}
	
    /**
     * Crée une nouvelle commande en utilisant les informations fournies dans le formulaire.
     *
     * @param session La session HTTP utilisée pour stocker des informations temporaires.
     * @param form    Le formulaire contenant les informations de la nouvelle commande.
     * @param bindingResult Le résultat de la liaison qui contient les erreurs de validation.
     * @param model   Le modèle utilisé pour transmettre des données à la vue.
     * @return Le nom de la vue à afficher après la création de la commande.
     */
	@PostMapping("/createOrder")
	public String createOrderAndSave(
			HttpSession session,
			@ModelAttribute("orderForm") @Validated OrderForm form,
			BindingResult bindingResult, 
			Model model) {
		
		if (!bindingResult.hasErrors()) {
			
		Integer customerId = form.getCustomerId();
		Customer customer = customerRepository
				.findById(customerId)
				.orElse(null);
		
		Order order = new Order();
		order.setAdrEt(form.getAdrEt());
		order.setCustomer(customer);
		order.setLabel(form.getLabel());
		order.setNotes(form.getNotes());
		order.setNumberOfDays(form.getNumberOfDays());
		order.setStatus(form.getStatus());
		order.setTva(form.getTva());
		order.setType(form.getType());
		order.setGuarantee(form.getGuarantee());
		order.setEstimate(form.getEstimate());
		
		orderRepository.save(order);
		System.out.println(order);
		
		model.addAttribute("order", order);
		
		Map<Integer, Order> orders = (Map<Integer, Order>) session.getAttribute(SESSION_ORDERS);
		
		if (orders == null) {
			orders = new HashMap<>();
		}
		 orders.put(order.getId(), order);
		 
		 session.setAttribute(SESSION_ORDERS, orders);
		 
		 System.out.println("*** Pas d'erreur : redirection vers /listOrders ***");
			return "redirect:/" + PAGE_ORDERS_LIST;
		} else {
			System.out.println("*** Erreur : redirection vers /createOrders ***");
			return "redirect:/" + PAGE_ORDER_CREATE;
		}
	}
	
}
