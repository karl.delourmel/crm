package fr.wijin.crm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * La classe principale de l'application Spring Boot.
 * Elle est marquée avec l'annotation @SpringBootApplication, ce qui indique que c'est la classe de démarrage de l'application.
 */
@SpringBootApplication
public class SpringbootProjetFilRougeCrmApplication {
	
    /**
     * La méthode principale qui démarre l'application Spring Boot.
     *
     * @param args Les arguments de la ligne de commande qui peuvent être passés au démarrage de l'application.
     */

	public static void main(String[] args) {
		SpringApplication.run(SpringbootProjetFilRougeCrmApplication.class, args);
	}

}
