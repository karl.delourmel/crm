/* Insertion du fichier client test */
insert into client (
    nom, prenom, entreprise, telephone_fixe, telephone_mobile, email, commentaire, etat)
values
    ('durant', 'pierre', 'cogip', '0625248798', '0299562878', 'durant@aol.fr', 'machin truc', 1),
    ('duce', 'jean-claude', 'baton eurl', '0658496887', '0299568745', 'jean@lycos.fr', 'azert', 0),
    ('rotrude', 'charloupe', 'ANPE', '0658963214', '0299654787', 'rotrude@club-internet.fr', 'heuuuuuuuu', 1),
    ('jean', 'henri', 'cogip', '0625248798', '0299562878', 'durant@aol.fr', 'machin truc', 1),
    ('claude', 'zaza', 'truc', '0658496887', '0299568745', 'jean@lycos.fr', 'azertdfs', 0),
    ('vincent', 'charloupe', 'ANPE', '0658963214', '0299654787', 'rotrude@club-internet.fr', 'heuuuuu', 1)
;